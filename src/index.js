const Redis = require('ioredis');
const NodeCache = require('node-cache');
const Pulsar = require('pulsar-client');

// ENV
const PULSAR_SERVICE_URL_DC1 = process.env?.PULSAR_SERVICE_URL_DC1 || 'pulsar://localhost:6650';
const PULSAR_SERVICE_URL_DC2 = process.env?.PULSAR_SERVICE_URL_DC2 || 'pulsar://localhost:6660';

const PULSAR_TOPIC_DC1 = process.env?.PULSAR_TOPIC_DC1 || 'persistent://minds-com/engine/redis-key-events';
const PULSAR_TOPIC_DC2 = process.env?.PULSAR_TOPIC_DC2 || 'persistent://minds-com/engine/redis-key-events';

const REDIS_HOST_DC1 = process.env?.REDIS_HOST_DC1 || 'localhost';
const REDIS_HOST_DC2 = process.env?.REDIS_HOST_DC2 || 'localhost';

const REDIS_PORT_DC1 = process.env?.REDIS_PORT_DC1 || 6379;
const REDIS_PORT_DC2 = process.env?.REDIS_PORT_DC2 || 6380;

const KEY_TTL = process.env?.KEY_TTL || 5;

// Configure node-cache
const nodeCache = new NodeCache();

// Configure Redis clients for both DCs
const redisDC1 = new Redis({ host: REDIS_HOST_DC1, port: REDIS_PORT_DC1 });
const redisDC2 = new Redis({ host: REDIS_HOST_DC2, port: REDIS_PORT_DC2 });

// Configure Pulsar clients for both DCs
const pulsarDC1 = new Pulsar.Client({
  serviceUrl: PULSAR_SERVICE_URL_DC1,
  useTls: true,
  tlsValidateHostname: false,
  tlsAllowInsecureConnection: true
});

const pulsarDC2 = new Pulsar.Client({
  serviceUrl: PULSAR_SERVICE_URL_DC2,
  useTls: true,
  tlsValidateHostname: false,
  tlsAllowInsecureConnection: true
});

// Define the event handler for both DCs
const onEvent = (msg, source, dest) => {
  // dc;cmd;key
  const data = msg.getData().toString().split(';');
  const dc = data[0];
  const cmd = data[1];
  const key = data[2];

  const cacheKey = `${cmd}:${key}`;

  // Check if the key has been locked
  if (nodeCache.get(cacheKey) === true) {
    console.log(`Key ${cacheKey} has been acked, removing from cache`);
    nodeCache.del(cacheKey); // Unlock the key
    return;
  }

  switch (cmd) {
    case 'set':
      console.log(`SET event for key: ${key} in DC: ${dc}`);
      nodeCache.set(cacheKey, true, KEY_TTL); // Lock the key
      source.get(key).then((value) => {
        console.log("value: " + value);
        dest.set(key, value);   
      });
      break;
    case 'del':
      console.log(`DEL event for key: ${key} in DC: ${dc}`);
      nodeCache.set(cacheKey, true, KEY_TTL); // Lock the key
      dest.del(key);
      break;
    case 'expire':
      console.log(`EXPIRE event for key: ${key} in DC: ${dc}`);
      nodeCache.set(cacheKey, true, KEY_TTL); // Lock the key
      source.ttl(key).then((ttl) => {
        console.log("TTL: " + ttl);
        dest.expire(key, ttl);   
      });
      break;
    default:
      break;
  }
};

// Create Pulsar listeners for both DCs
pulsarDC1.subscribe({
  topic: PULSAR_TOPIC_DC1,
  subscription: 'redis-geo-sync',
  ackTimeoutMs: 10000,
  startMessageId: Pulsar.MessageId.earliest(),
  listener: (msg, msgConsumer) => {
    console.log(`DC1: ${msg.getData().toString()}`);
    onEvent(msg, redisDC1, redisDC2);
    msgConsumer.acknowledge(msg);
  },
});

pulsarDC2.subscribe({
  topic: PULSAR_TOPIC_DC2,
  subscription: 'redis-geo-sync',
  ackTimeoutMs: 10000,
  startMessageId: Pulsar.MessageId.earliest(),
  listener: (msg, msgConsumer) => {
    console.log(`DC2: ${msg.getData().toString()}`);
    onEvent(msg, redisDC2, redisDC1);
    msgConsumer.acknowledge(msg);
  },
});
