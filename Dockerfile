FROM node:lts-alpine

RUN apk update && apk upgrade --no-cache

WORKDIR /opt/app

COPY package.json package-lock.json ./
COPY src/ ./src/

RUN npm i --ommit=dev

CMD ["npm", "run", "start"]